<div class="page-wrapper">
  <div class="page-wrapper-sidebar">
    <?php print _wplike_get_sidebar() ?>
  </div>
  <div class="page-wrapper-content">
    <?php print $children; ?>
  </div>
</div>

